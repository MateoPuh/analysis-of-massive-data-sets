import sys

l = sys.stdin.readline().rstrip('\n').split()
n = int(l[0])
beta = float(l[1])

ingoing = {}
outgoing = {}
k = {}

for i in range(n):
    line = sys.stdin.readline().rstrip('\n').split()

    outgoing[i] = line
    k[i] = len(line)

    for a in line:
        ingoing.setdefault(int(a), []).append(i)

q = int(sys.stdin.readline())

queries = []

for qi in range(q):
    l = sys.stdin.readline().rstrip('\n').split()
    ni = int(l[0])
    ti = int(l[1])

    queries.append((ni, ti))

r = [[1 / n] * n]

for t in range(1, 101):
    r.append([0] * n)
    s = 0
    for j in range(n):
        if j in ingoing:
            for i in ingoing[j]:
                r[t][j] += beta * (r[t - 1][i] / k[i])
            s += r[t][j]

    for j in range(n):
        r[t][j] += (1 - s) / n

for query in queries:
    print("%.10f" % r[query[1]][query[0]])
