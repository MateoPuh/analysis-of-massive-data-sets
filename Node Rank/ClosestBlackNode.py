import sys

def closest_black_node(graph, i, depth=0):
    bg = -1
    distg = -1

    if depth > 10:
        return bg, distg

    nodes = graph[i]

    for ni in nodes:
        if types[ni] == 1:
            if bg == -1 or ni < bg:
                bg = ni
                distg = depth+1

    if bg != -1:
        return bg, distg

    for j in graph[i]:
        b2, dist2 = closest_black_node(graph, j, depth + 1)

        if b2 == -1:
            continue

        if dist2 == distg:
            if bg == -1 or b2 < bg:
                bg = b2

        if distg == -1 or dist2 < distg:
            distg = dist2
            bg = b2

    return bg, distg


l = sys.stdin.readline().rstrip('\n').split()
n = int(l[0])
e = int(l[1])

types = []
graph = {}

for i in range(n):
    line = sys.stdin.readline().rstrip('\n').split()

    t = int(line[0])
    types.append(t)

for i in range(e):
    line = sys.stdin.readline().rstrip('\n').split()

    s = int(line[0])
    d = int(line[1])
    graph.setdefault(s, []).append(d)
    graph.setdefault(d, []).append(s)

for i in range(n):
    if types[i] == 1:
        print(i, 0, sep=" ")
    else:
        b, dist = closest_black_node(graph, i)
        print(b, dist, sep=" ")

