import java.util.*;
import java.util.stream.Collectors;

public class PCY {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        scanner.nextLine();

        double s = scanner.nextDouble();
        scanner.nextLine();

        int b = scanner.nextInt();
        scanner.nextLine();

        int prag = (int) Math.floor(s * N);

        String[] kosare = new String[N];
        Map<Integer, Integer> brojac = new HashMap<>();

        // prvi prolaz
        for (int i = 0; i < N; i++) {
            kosare[i] = scanner.nextLine();
            String[] predmeti = kosare[i].split(" ");

            for (String predmet : predmeti) {
                int p = Integer.parseInt(predmet.trim());
                brojac.merge(p, 1, ((i1, i2) -> i1 + 1));
            }
        }

        long m = brojac.entrySet().stream().filter(e -> e.getValue() >= prag).count();

        int k = brojac.size();
        int[] pretinci = new int[b];

        // drugi prolaz
        for (String kosara : kosare) {
            String[] predmeti = kosara.split(" ");
            List<Integer> p = Arrays.stream(predmeti).map(Integer::parseInt).collect(Collectors.toList());

            // za svaki par pi, pj unutar kosare
            for (int i = 0; i < p.size(); i++) {
                int pi = p.get(i);
                for (int j = i + 1; j < p.size(); j++) {
                    int pj = p.get(j);

                    if (brojac.get(pi) >= prag && brojac.get(pj) >= prag) {
                        int h = (pi * brojac.size() + pj) % b;
                        pretinci[h]++;
                    }
                }
            }
        }

        Map<Pair, Integer> parovi = new HashMap<>();

        for (String kosara : kosare) {
            String[] predmeti = kosara.split(" ");
            List<Integer> p = Arrays.stream(predmeti).map(Integer::parseInt).collect(Collectors.toList());

            for (int i = 0; i < p.size(); i++) {
                int pi = p.get(i);
                for (int j = i + 1; j < p.size(); j++) {
                    int pj = p.get(j);

                    if (brojac.get(pi) >= prag && brojac.get(pj) >= prag) {
                        int h = (pi * brojac.size() + pj) % b;

                        if (pretinci[h] >= prag) {
                            parovi.merge(new Pair(p.get(i), p.get(j)), 1, ((i1, i2) -> i1 + 1));
                        }
                    }
                }
            }
        }

        System.out.println(m * (m - 1) / 2);
        System.out.println(parovi.size());

        parovi.entrySet().stream().sorted(Map.Entry.comparingByValue(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1.equals(o2)) return 0;
                return o1 > o2 ? -1 : 1;
            }
        })).forEach(e -> System.out.println(e.getValue()));
    }

    private static int h(int i, int j, int k, int b) {
        return (i * k + j) % b;
    }

    private static class Pair {

        private int a;
        private int b;

        public Pair(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return a == pair.a &&
                    b == pair.b;
        }

        @Override
        public int hashCode() {
            return Objects.hash(a, b);
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "a=" + a +
                    ", b=" + b +
                    '}';
        }
    }
}
