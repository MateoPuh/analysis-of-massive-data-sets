import sys
import numpy as np
import math
from decimal import Decimal, ROUND_HALF_UP

l = sys.stdin.readline().rstrip('\n').split()
n = int(l[0]) # br stavki
m = int(l[1]) # br korisnika

userItems = np.zeros((n, m))

for i in range(n):
    l = sys.stdin.readline().rstrip('\n').split()

    j = 0
    for li in l:
        if li == 'X':
            userItems[i, j] = 0
        else:
            userItems[i, j] = int(li)

        j += 1

userItemsII = np.zeros((n, m))

for item in range(n):
    summ = 0
    cnt = 0
    for ij in range(m):
        if userItems[item, ij] == 0:
            continue

        summ += userItems[item, ij]
        cnt += 1

    avg = summ/cnt
    for ij in range(m):
        if userItems[item, ij] == 0:
            userItemsII[item, ij] = 0
            continue
        userItemsII[item, ij] = userItems[item, ij] - avg

userItemsUU = np.zeros((n, m))

for user in range(m):
    summ = 0
    cnt = 0
    for ji in range(n):
        if userItems[ji, user] == 0:
            continue

        summ += userItems[ji, user]
        cnt += 1

    avg = summ/cnt
    for ji in range(n):
        if userItems[ji, user] == 0:
            userItemsUU[ji, user] = 0
            continue
        userItemsUU[ji, user] = userItems[ji, user] - avg

simII = {}

for i1 in range(n):
    for i2 in range(n):
        s1 = 0
        s2 = 0
        s3 = 0
        for j in range(m):
            s1 += userItemsII[i1, j] * userItemsII[i2, j]
            s2 += userItemsII[i1, j] ** 2
            s3 += userItemsII[i2, j] ** 2

        simII[(i1, i2)] = s1 / math.sqrt(s2 * s3)

simUU = {}

for j1 in range(m):
    for j2 in range(m):
        s1 = 0
        s2 = 0
        s3 = 0
        for i in range(n):
            s1 += userItemsUU[i, j1] * userItemsUU[i, j2]
            s2 += userItemsUU[i, j1] ** 2
            s3 += userItemsUU[i, j2] ** 2

        simUU[(j1, j2)] = s1 / math.sqrt(s2 * s3)

# print(userItems)
# print(userItemsII)
# print(userItemsUU)
#
# print(simII)
# print(simUU)

q = int(sys.stdin.readline())

for qi in range(q):
    l = sys.stdin.readline().rstrip('\n').split()
    i = int(l[0]) - 1
    j = int(l[1]) - 1
    t = int(l[2])
    k = int(l[3])

    if t == 0:
        # item-item
        sum1 = 0
        simsum = 0

        simi = {}

        for i2 in range(n):
            if i == i2:
                continue
            simi[i2] = simII[(i, i2)]

        index = 0
        for w in sorted(simi, key=simi.get, reverse=True):
            if index >= k:
                break

            if simi[w] <= 0:
                break

            if userItems[w, j] != 0:
                sum1 += userItems[w, j] * simi[w]
                simsum += simi[w]
                index += 1

        print(Decimal(Decimal(sum1 / simsum).quantize(Decimal('.001'), rounding=ROUND_HALF_UP)))

    else:
        # user-user
        sum1 = 0
        simsum = 0

        simj = {}

        for j2 in range(m):
            if j == j2:
                continue
            simj[j2] = simUU[(j, j2)]

        index = 0
        for w in sorted(simj, key=simj.get, reverse=True):
            if index >= k:
                break

            if simj[w] <= 0:
                break

            if userItems[i, w] != 0:
                sum1 += userItems[i, w] * simj[w]
                simsum += simj[w]
                index += 1

        print(Decimal(Decimal(sum1 / simsum).quantize(Decimal('.001'), rounding=ROUND_HALF_UP)))
