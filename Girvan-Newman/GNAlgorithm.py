'''
edges = G.get_edges()
while edges:
edge_betweenness = calculate_betweenness(G)
edges_to_remove = get_edges_with_highest_betweenness(edge_betweenness)
for edge in edges_to_remove:
remove_edge(edge, G)
edges = G.get_edges()
'''

import sys


def is_empty(edges):
    if len(edges) == 0:
        return True

    empty = True
    for e in edges.keys():
        if len(edges[e]) != 0:
            empty = False

    return empty


def get_partition(edges, start, visited=[]):
    if start in visited:
        return None

    partitionp = [start]

    visited.append(start)

    if start in edges:
        for e in edges[start]:
            if e not in visited:
                p = get_partition(edges, e, visited)
                if p is not None:
                    for part in p:
                        partitionp.append(part)

    return partitionp


def calculate_modularity(edges):
    m = 0
    k = {}
    for u in edges.keys():
        ku = 0
        for v in edges[u]:
            m += edges[u][v]
            ku += edges[u][v]
        k[u] = ku

    s = 0
    for u in edges.keys():
        for v in edges[u]:
            s += edges[u][v] - (k[u] * k[v] / (2 * m))

    q = 1/(2 * m) * s
    return q


def calculate_weights(edges, attrs):
    # racucanje tezina
    max = len(attrs[list(attrs.keys())[0]])

    for n in edges:
        for n2 in edges[n]:
            sim = len(list(filter(lambda a: a[0] == a[1], zip(attrs[n], attrs[n2])))) - 1

            edges[n][n2] = max - sim


def calculate_central(edges, attrs):
    # racunanje centralnosti
    for n in attrs.keys():
        for n2 in attrs.keys():
            if n != n2:
                paths, dist = find_shortest_paths(edges, n, n2)
                if paths is None:
                    continue
                num_of_paths = len(paths)

                for path in paths:
                    for i in range(1, len(path)):
                        central.setdefault(path[i - 1], {}).setdefault(path[i], 0)
                        central[path[i - 1]][path[i]] += 1 / (2 * num_of_paths)


def find_max_central(central):
    max = -1.0
    max_edges = None
    for c in central.keys():
        for c2 in central[c]:
            if max == -1:
                max = central[c][c2]

                if c < c2:
                    max_edges = [(c, c2)]
                else:
                    max_edges = [(c2, c)]

            if central[c][c2] == max:
                if c < c2:
                    max_edges.append((c, c2))
                else:
                    max_edges.append((c2, c))

            if central[c][c2] > max:
                max = central[c][c2]
                if c < c2:
                    max_edges = [(c, c2)]
                else:
                    max_edges = [(c2, c)]
    return max_edges, max


def find_shortest_paths(graph, start, end):
    paths = find_all_paths(graph, start, end)
    if len(paths) == 0:
        return None, None

    lengths = [sum([edges[path[i - 1]][path[i]] for i in range(1, len(path))]) for path in paths]
    min_dist = min(lengths)
    shortests = [paths[i] for i, l in enumerate(lengths) if l == min_dist]
    return shortests, min_dist

def find_all_paths(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return [path]
    if start not in graph.keys():
        return []
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths


edges = {}
central = {}
attrs = {}

for line in sys.stdin:
    l = line.rstrip('\n').split()

    if not l:
        break

    id1 = int(l[0])
    id2 = int(l[1])

    edges.setdefault(id1, {})[id2] = 1
    edges.setdefault(id2, {})[id1] = 1


for line in sys.stdin:
    l = line.rstrip('\n').split()

    if not l:
        break

    id = int(l[0])
    id_attrs = [int(attr) for attr in l[1:]]

    attrs[id] = id_attrs

max_q = 0
max_q_edges = None

while not is_empty(edges):
    calculate_weights(edges, attrs)
    central = {}
    calculate_central(edges, attrs)
    max_edges, max = find_max_central(central)

    q = calculate_modularity(edges)
    if q > max_q:
        max_q = q
        max_q_edges = {}

        for e in edges.keys():
            max_q_edges.setdefault(e, {})
            for e2 in edges[e]:
                max_q_edges[e].setdefault(e2, {})
                max_q_edges[e][e2] = edges[e][e2]

    for edge in sorted(max_edges):
        if edge[0] in edges and edge[1] in edges and edge[1] in edges[edge[0]] and edge[0] in edges[edge[1]]:
            print(edge[0], edge[1])
            del edges[edge[0]][edge[1]]
            del edges[edge[1]][edge[0]]

# print(max_q_edges)

visited = []

partitions = []

for e in sorted(attrs.keys()):
    if e not in visited:
        p = get_partition(max_q_edges, e, visited)

        if p is not None:
            partitions.append(p)

for partition in sorted(partitions, key = lambda p: len(p)):
    print('%s' % '-'.join(map(str, sorted(partition))), end=' ')
