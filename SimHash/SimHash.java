import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SimHash {

    public static String simHash(String text) {
        int[] sh = new int[128];
        String[] parts = text.split(" ");

        for (String part : parts) {
            byte[] h = DigestUtils.md5(part);

            StringBuilder sb = new StringBuilder();

            for (byte b : h) {
                String binPart = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
                sb.append(binPart);
            }

            String binary = sb.toString();

            for (int i = 0; i < 128; i++) {
                if (binary.charAt(i) == '1') {
                    sh[i]++;
                } else {
                    sh[i]--;
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        for (int s : sh) {
            if (s >= 0) {
                sb.append(1);
            } else {
                sb.append(0);
            }
        }

        return sb.toString();
    }

    public static int diff(String s1, String s2) {
        int diff = 0;

        for (int i = 0; i < 128; i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                diff++;
            }
        }

        return diff;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        sc.nextLine();

        List<String> hashes = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            hashes.add(simHash(sc.nextLine()));
        }

        int q = sc.nextInt();
        sc.nextLine();

        for (int j = 0; j < q; j++) {
            int i = sc.nextInt();
            int k = sc.nextInt();

            String hash = hashes.get(i);

            int res = 0;

            for (int index = 0; index < n; index++) {
                if (index == i) continue;
                String h = hashes.get(index);

                if (diff(hash, h) <= k) {
                    res += 1;
                }
            }

            System.out.println(res);
        }
    }
}
