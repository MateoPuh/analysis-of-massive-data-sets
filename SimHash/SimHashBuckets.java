import org.apache.commons.codec.digest.DigestUtils;

import java.util.*;

public class SimHashBuckets {

    private static int b = 8;

    public static String simHash(String text) {
        int[] sh = new int[128];
        String[] parts = text.split(" ");

        for (String part : parts) {
            byte[] h = DigestUtils.md5(part);

            StringBuilder sb = new StringBuilder();

            for (byte b : h) {
                String binPart = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
                sb.append(binPart);
            }

            String binary = sb.toString();

            for (int i = 0; i < 128; i++) {
                if (binary.charAt(i) == '1') {
                    sh[i]++;
                } else {
                    sh[i]--;
                }
            }
        }

        StringBuilder sb = new StringBuilder();

        for (int s : sh) {
            if (s >= 0) {
                sb.append(1);
            } else {
                sb.append(0);
            }
        }

        return sb.toString();
    }

    public static int diff(String s1, String s2) {
        int diff = 0;

        for (int i = 0; i < 128; i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                diff++;
            }
        }

        return diff;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        sc.nextLine();

        List<String> hashes = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            hashes.add(simHash(sc.nextLine()));
        }

        HashMap<Integer, List<Integer>> candidates = new HashMap<>();

        for (int i = 0; i < b; i++) {
            HashMap<Integer, List<Integer>> pretinci = new HashMap<>();

            for (int index = 0; index < n; index++) {
                String hash = hashes.get(index);
                int val = Integer.parseInt(hash.substring(i * 16, (i + 1) * 16), 2);

                List<Integer> texts = new ArrayList<>();

                if (pretinci.containsKey(val)) {
                    texts = pretinci.get(val);

                    for (Integer hashIndex : texts) {
                        if (!candidates.containsKey(hashIndex)) {
                            candidates.put(hashIndex, new ArrayList<>());
                        }
                        if (!candidates.containsKey(index)) {
                            candidates.put(index, new ArrayList<>());
                        }

                        if (!candidates.get(hashIndex).contains(index)) {
                            candidates.get(hashIndex).add(index);
                        }

                        if (!candidates.get(index).contains(hashIndex)) {
                            candidates.get(index).add(hashIndex);
                        }
                    }
                }

                texts.add(index);

                pretinci.put(val, texts);
            }
        }

        int q = sc.nextInt();
        sc.nextLine();

        for (int j = 0; j < q; j++) {
            int i = sc.nextInt();
            int k = sc.nextInt();

            String hash = hashes.get(i);

            int res = 0;

            for (Integer index : candidates.get(i)) {
                String h = hashes.get(index);

                if (diff(hash, h) <= k) {
                    res += 1;
                }
            }

            System.out.println(res);
        }
    }
}
